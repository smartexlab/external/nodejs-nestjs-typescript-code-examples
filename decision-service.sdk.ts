import * as _ from 'lodash';
import { Injectable } from '@nestjs/common';
import { Resource, MovementType } from '../entities';
import { ValidationException } from '../../../common/exceptions';
import { ERROR_CODES } from '../../../common/exceptions/error-codes';
import { InternalServerException } from '../../../common/exceptions/internal-server.exception';

export interface TargetStorageBin {
    priority: number;
    label: string;
}

export interface MakeDecisionResponseItem {
    target_storage_bins: TargetStorageBin[];
}

export enum DecisionServiceMSCommands {
    MAKE_DECISION = 'decision-service#makeDecision',
    GET_SCHEMA_BY_METADATA = 'decision-service#getDecisionSchemaByMetadata',
}

@Injectable()
export class DecisionServiceSDK {
    protected readonly logger = new LoggerService('inventory-service#DecisionSDK');

    public constructor(private readonly microserviceProvider: MicroserviceProvider) {}

    public async getStorageBinFromDecisionService(
        foundResource: Resource,
        siteStorageBinLabel: string,
        action: MovementType,
        companyId: string,
    ): Promise<TargetStorageBin[]> {
        const decisionSchema = await this.microserviceProvider.sendCmd(DecisionServiceMSCommands.GET_SCHEMA_BY_METADATA, {
            application: 'INVENTORY_SERVICE',
            reference: siteStorageBinLabel,
            action,
            companyId,
        });

        if (!decisionSchema.id) {
            throw new ValidationException([
                {
                    entityName: 'Movement',
                    errorType: ERROR_CODES.DECISION_SERVICE_RETURNS_ERROR_DURING_RETRIEVING_DECISION_SCHEMA.techName,
                    propertyName: 'siteStorageBinLabel',
                },
            ]);
        }

        let targetStorageBins: MakeDecisionResponseItem[];

        try {
            targetStorageBins = await this.microserviceProvider.sendCmd<MakeDecisionResponseItem[]> (
                DecisionServiceMSCommands.MAKE_DECISION,
                {
                    companyId,
                    conditions: {
                        resource_label: foundResource.label,
                        resource_kind: foundResource.kind,
                        resource_loading_status: foundResource.loadingStatus,
                },
                decisionSchemaId: decisionSchema.id,
            });
        } catch (error) {
            throw new InternalServerException('Movement', ERROR_CODES.DECISION_SERVICE_RETURNS_ERROR.techName, 'targetStorageBinId');
        }

        if (!Array.isArray(targetStorageBins)) {
            return;
        }

        return _.sortBy(targetStorageBins[0].target_storage_bins, 'priority');
    }
}
