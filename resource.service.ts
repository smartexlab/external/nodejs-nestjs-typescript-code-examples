import * as _ from 'lodash';
import { ObjectID } from 'bson';
import * as csv from 'csvtojson';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ModelType, InstanceType } from 'typegoose';
import { Resource, ResourceType, RESOURCE_VOLUME_MEASUREMENT_TYPE, MovementType, Movement } from '../entities';
import { ERROR_CODES } from '../../../common/exceptions/error-codes';
import { EntityNotFoundException, ValidationException } from '../../../common/exceptions';
import { MovementService } from './movement.service';
import { GetResourceDto } from '../dtos/resources/get-resource.dto';
import { isItemBelongsToCompany } from '../../../common/utils/belonging-to-company-validator.util';
import { NotAllowedException } from '../../../common/exceptions/not-allowed.exception';
import { TransactionsService } from '../../transactions/transactions.service';
import { StorageBinService } from './storage-bin.service';
import { reflect } from '../../../common/utils/reflect-promise.util';
import { PaginateOptions } from '../../../common/utils/mongoose-pagination.utils';
import { ClientSession } from 'mongoose';

@Injectable()
export class ResourceService {
    public constructor(
        @InjectModel(Resource.modelName) private readonly resource: ModelType<Resource>,
        private readonly transactionService: TransactionsService,
        private readonly movementService: MovementService,
        private readonly storageBinService: StorageBinService,
    ) {}

    public async create(resourceCreate: Resource, companyId: string): Promise<Resource> {
        if (!resourceCreate.volumeMeasurementType) {
            resourceCreate.volumeMeasurementType = Resource.getDefaultVolumeMeasurementType(resourceCreate.kind);
        }
        resourceCreate.volume = Resource.getDefaultVolume(resourceCreate.kind, resourceCreate.volumeMeasurementType);
        resourceCreate.companyId = companyId;

        const response = await this.resource.create(resourceCreate);
        return response;
    }

    public async update(id: string, updateResourceBody: Partial<Resource>, companyId: string, session?: ClientSession): Promise<Resource> {
        const clientSession = session ? session : await this.transactionService.startTransactionSession();
        const existingResource: InstanceType<Resource> = await this.getInstanceById(id, companyId, clientSession);

        if (updateResourceBody.kind && updateResourceBody.kind !== existingResource.kind && isResourceAlreadyCoupled(existingResource)) {
            throw new ValidationException([{
                entityName: 'Resources',
                errorType: ERROR_CODES.FORBIDS_TO_UPDATE_KIND_OF_ALREADY_COUPLED_RESOURCE.techName,
                propertyName: 'kind',
            }]);
        }

        existingResource.set(updateResourceBody);

        const updatedResource = await existingResource.save();

        if (!session) {
            await this.transactionService.commitTransaction(clientSession);
        }
        return updatedResource;
    }

    public async getInstanceById(id: string, companyId: string, session?: ClientSession): Promise<InstanceType<Resource>> {
        const clientSession = session ? session : await this.transactionService.startTransactionSession();
        const resource: InstanceType<Resource> = await this.resource.findById(id).session(clientSession).exec();

        if (!resource) {
            throw new EntityNotFoundException('Resource', 'id');
        }

        if (!isItemBelongsToCompany(resource, companyId)) {
            throw new NotAllowedException('Resource', 'companyId');
        }

        if (!session) {
            await this.transactionService.commitTransaction(clientSession);
        }
        return resource;
    }

    public async getAll(navOptions: NavOptions, companyId: string, siteId?: string): Promise<NavListDTO<{}>> {
        if (!navOptions.limit) {
            navOptions.limit = 100;
        }

        if (!navOptions.skip) {
            navOptions.skip = 0;
        }

        const queryParams = navOptions.filter || {};
        queryParams.companyId = companyId;
        queryParams.archived = { $ne: true };

        const paginationOptions: PaginateOptions = {
            offset: navOptions.skip,
            limit: navOptions.limit,
        };
        if (navOptions.sort) {
            Object.assign(paginationOptions, { sort: navOptions.sort });
        }

        if (navOptions.expand) {
            if (navOptions.expand.stocked) {
                queryParams.locatedOn = { $exists: true, $ne: null };
            }

            if (navOptions.expand.unstocked) {
                queryParams.locatedOn = null;
            }

            if (navOptions.expand.archived) {
                queryParams.archived = true;
            }

            if (navOptions.expand.populateCoupled) {
                paginationOptions.populate = [
                    {
                        path: 'coupledResources',
                        populate: {
                            path: 'coupledResources',
                            populate: {
                                path: 'coupledResources',
                            },
                        },
                    },
                ];
            }
        }
        const storageBinNavOptions = new NavOptions(Number.MAX_SAFE_INTEGER, 0);

        if (siteId) {
            const includedStorageBins = await this.storageBinService.findAllIncludedStorageBins(siteId, storageBinNavOptions, companyId);
            queryParams.locatedOn = {
                $in: includedStorageBins.data.map(storageBin => storageBin.id.toString()),
            };
        }

        const result = await this.resource.paginate(queryParams, paginationOptions);

        return new NavListDTO()
            .setData(result.docs.map(itemEntity => GetResourceDto.fromEntity(itemEntity)))
            .setPageSize(navOptions.limit)
            .setPageNumberFromSkip(navOptions.skip)
            .setTotalSize(result.total || 0);
    }

    public async removeResourceById(id: string, companyId: string,  userId: string) {
        const clientSession = await this.transactionService.startTransactionSession();
        const resourceToRemove: InstanceType<Resource> = await this.resource
            .findById(id)
            .session(clientSession)
            .exec();

        if (!isItemBelongsToCompany(resourceToRemove, companyId)) {
            throw new NotAllowedException('Resource', 'companyId');
        }

        if (!resourceToRemove) {
            throw new EntityNotFoundException('Resource', 'id');
        }

        if (resourceToRemove.locatedOn) {
            await this.movementService.create(
                Movement.Create({
                    movementType: MovementType.REMOVE_STOCK,
                    movementInfo: {
                        resourceId: resourceToRemove.id,
                        storageBinId: resourceToRemove.locatedOn,
                    },
                }), companyId, userId, clientSession);
        }
        if (resourceToRemove.reservedStorageBin) {
            await this.movementService.create(
                Movement.Create({
                    movementType: MovementType.CANCEL_RESERVATION,
                    movementInfo: {
                        resourceId: resourceToRemove.id,
                        destinationStorageBinId: resourceToRemove.reservedStorageBin,
                    },
                }), companyId, userId, clientSession);
        }

        if (resourceToRemove.coupledResources.length) {
            await Promise.all(
                resourceToRemove.coupledResources.map(coupledResource =>
                    this.movementService.create(
                        Movement.Create({
                            movementType: MovementType.UNCOUPLING,
                            movementInfo: {
                                masterResourceId: resourceToRemove.id,
                                slaveResourceId: coupledResource,
                            },
                        }),
                        companyId,
                        userId,
                        clientSession,
                    ),
                ),
            );
        }

        if (resourceToRemove.masterResource) {
            await this.movementService.create(
                Movement.Create({
                    movementType: MovementType.UNCOUPLING,
                    movementInfo: {
                        masterResourceId: resourceToRemove.masterResource,
                        slaveResourceId: resourceToRemove.id,
                    },
                }),
                companyId,
                userId,
                clientSession,
            );
        }
        await this.resource
            .findByIdAndRemove(resourceToRemove.id)
            .session(clientSession)
            .exec();
        await clientSession.commitTransaction();
        return resourceToRemove;
    }

    public async archiveResource(id: string, companyId: string, userId: string, session?: ClientSession) {
        const clientSession = session ? session : await this.transactionService.startTransactionSession();
        const updatedResource = await this.update(id, { archived: true }, companyId, clientSession);

        if (updatedResource.masterResource) {
            await this.movementService.create(
                Movement.Create({
                    movementType: MovementType.UNCOUPLING,
                    movementInfo: {
                        masterResourceId: updatedResource.masterResource,
                        slaveResourceId: updatedResource.id,
                    },
                }),
                companyId,
                userId,
                clientSession,
            );
        }

        if (updatedResource.locatedOn) {
            await this.movementService.create(
                Movement.Create({
                    movementType: MovementType.REMOVE_STOCK,
                    movementInfo: {
                        resourceId: updatedResource.id,
                        storageBinId: updatedResource.locatedOn,
                    },
                }), companyId, userId, clientSession);
        }
        if (updatedResource.reservedStorageBin) {
            await this.movementService.create(
                Movement.Create({
                    movementType: MovementType.CANCEL_RESERVATION,
                    movementInfo: {
                        resourceId: updatedResource.id,
                        destinationStorageBinId: updatedResource.reservedStorageBin,
                    },
                }), companyId, userId, clientSession);
        }
        if (updatedResource.coupledResources.length) {
            await Promise.all(updatedResource.coupledResources.map(resource => this.archiveResource(resource.toString(), companyId,
                userId, clientSession)));
        }

        if (!session) {
            await this.transactionService.commitTransaction(clientSession);
        }
        return updatedResource;
    }

    public async createTrailersStockedWithStorageBinsByCSV(file: any, companyId: string, userId: string): Promise<{
        successful: number;
        rejected: number;
    }> {
        const clientSession = await this.transactionService.startTransactionSession();
        const results = await csv({
            trim: true,
        }).fromString(file.buffer.toString('UTF-8'));
        if (!results || !results.length) {
            throw new ValidationException([
                {
                    entityName: 'Resources',
                    errorType: ERROR_CODES.NON_PARSABLE_CSV_FILE.techName,
                    propertyName: 'file',
                },
            ]);
        }
        const filteredRows = results.filter(row => row.Trailers);
        let resources: InstanceType<Resource>[];
        try {
            const resourcesToCreate = filteredRows.map(row => ({
                _id: new ObjectID().toHexString(),
                kind: ResourceType.TRUCK,
                label: row.Trailers,
                companyId,
                volume: 1,
                archived: false,
                volumeMeasurementType: RESOURCE_VOLUME_MEASUREMENT_TYPE[ResourceType.TRUCK],
            }));

            resources = await this.resource.insertMany(resourcesToCreate, { session: clientSession });

            if (resources.length < resourcesToCreate.length) {
                throw new ValidationException([
                    {
                        entityName: 'Resources',
                        errorType: ERROR_CODES.MONGO_CAN_NOT_INSERT_VALUES.techName,
                        propertyName: 'csv',
                    },
                ]);
            }

            const storageBinLabels = filteredRows.map(row => row.name);
            const storageBins = await this.storageBinService.getStorageBinsByLabels(storageBinLabels);

            const retrievedStorageBinLabels = storageBins.map(storageBin => storageBin.label);
            const rowsToStock = filteredRows.filter(row => retrievedStorageBinLabels.includes(row.name));

            const movementsToCreate = rowsToStock.map(row => ({
                movementType: MovementType.ADD_STOCK,
                movementInfo: {
                    resourceId: resources.find(resource => resource.label === row.Trailers),
                    storageBinId: storageBins.find(storageBin => storageBin.label === row.name),
                },
                companyId,
            }));

            const movements = await Promise.all(
                movementsToCreate.map(
                movement => reflect(this.movementService.create(Movement.Create(movement), companyId, userId, clientSession))),
            );

            const fulfilledMovementsLength = movements
                .map(result => result.status)
                .filter(result => result === 'fulfilled')
                .length;

            await this.transactionService.commitTransaction(clientSession);
            return {
                successful: fulfilledMovementsLength,
                rejected: movements.length - fulfilledMovementsLength,
            };
        } catch (error) {
            await this.transactionService.abortTransaction(clientSession);
            throw error;
        }
    }
}
function isResourceAlreadyCoupled(existingResource: InstanceType<Resource>) {
    return existingResource.coupledResources.length > 0 || existingResource.masterResource;
}
