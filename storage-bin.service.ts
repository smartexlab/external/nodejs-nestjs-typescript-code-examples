import * as _ from 'lodash';
import { ObjectID } from 'bson';
import * as csv from 'csvtojson';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ModelType, InstanceType } from 'typegoose';
import { StorageBin, StorageBinType, Resource, StorageBinVolumeMeasurementType } from '../entities';
import { GetStorageBinDto } from '../dtos/storage-bin/get-storage-bin.dto';
import { ERROR_CODES } from '../../../common/exceptions/error-codes';
import { EntityNotFoundException, ValidationException } from '../../../common/exceptions';
import { NotAllowedException } from '../../../common/exceptions/not-allowed.exception';
import { isItemBelongsToCompany } from '../../../common/utils/belonging-to-company-validator.util';
import { UpdateStorageBinDto } from '../dtos/storage-bin/update-storage-bin.dto';
import { CreateStorageBinDto } from '../dtos/storage-bin/create-storage-bin.dto';
import { PaginateOptions } from '../../../common/utils/mongoose-pagination.utils';
import { ClientSession } from 'mongodb';
import { TransactionsService } from '../../transactions/transactions.service';
import { MovementType } from '../entities/movement.entity';
import { MovementService } from './movement.service';
import { CreateMovementDto } from '../dtos/movements/create-movement.dto';
import { UpdateOrderDto } from '../dtos/storage-bin/update-order.dto';

@Injectable()
export class StorageBinService {
    public constructor(
        @InjectModel(StorageBin.modelName) private readonly storageBin: ModelType<StorageBin>,
        @InjectModel(Resource.modelName) private readonly resource: ModelType<Resource>,
        private readonly movementService: MovementService,
        private readonly transactionService: TransactionsService,
    ) {}

    public async findAllIncludedStorageBins(
        rootStorageBinId: string,
        navOptions: NavOptions,
        companyId: string,
    ): Promise<NavListDTO<GetStorageBinDto[]>> {
        if (!navOptions.limit) {
            navOptions.limit = 100;
        }

        if (!navOptions.skip) {
            navOptions.skip = 0;
        }

        const aggregationPipeline: any[] = [
            {
                $match: {
                    _id: new ObjectID(rootStorageBinId),
                    companyId,
                },
            },
            {
                $graphLookup: {
                    from: 'storageBins',
                    startWith: '$_id',
                    connectFromField: '_id',
                    connectToField: 'masterStorageBinId',
                    as: 'storageBinHierarchy',
                },
            },
            {
                $unwind: '$storageBinHierarchy',
            },
            {
                $replaceRoot: {
                    newRoot: '$storageBinHierarchy',
                },
            },
        ];

        if (!navOptions.expand || !navOptions.expand.empty) {
            aggregationPipeline.push({
                $match: {
                    $or: [
                        {
                            stockedResources: { $exists: true, $ne: [] },
                        },
                        {
                            reservedStockResources: { $exists: true, $ne: [] },
                        },
                    ],
                },
            });
        }

        const paginationAggregationStage: any = {
            metadata: [{ $count: 'total' }],
            data: [],
        };

        if (Number.isInteger(navOptions.skip) && Number.isInteger(navOptions.limit)) {
            paginationAggregationStage.data.push(
                {
                    $skip: navOptions.skip,
                },
                {
                    $limit: navOptions.skip + navOptions.limit,
                },
            );
        }

        aggregationPipeline.push({
            $facet: paginationAggregationStage,
        });

        const result: any[] = await this.storageBin.aggregate(aggregationPipeline).exec();

        if (result.length > 0) {
            await this.resource.populate(result[0].data, {
                path: 'stockedResources',
                populate: {
                    path: 'coupledResources',
                    populate: {
                        path: 'coupledResources',
                    },
                },
            });

            await this.resource.populate(result[0].data, {
                path: 'reservedStockResources',
                populate: {
                    path: 'coupledResources',
                    populate: {
                        path: 'coupledResources',
                    },
                },
            });
        }

        return new NavListDTO<GetStorageBinDto[]>()
            .setData(result[0].data.map((itemEntity: StorageBin) => GetStorageBinDto.fromEntity(itemEntity)))
            .setPageSize(navOptions.limit)
            .setPageNumberFromSkip(navOptions.skip)
            .setTotalSize(_.get(result[0], 'metadata[0].total') || 0);
    }

    public async create(storageBinCreate: CreateStorageBinDto, companyId: string): Promise<StorageBin> {
        (storageBinCreate as any).companyId = companyId;

        if (storageBinCreate.masterStorageBinId) {
            await this._checkIfMasterStorageBinBelongsToCompany(storageBinCreate, companyId);
        }

        if (!storageBinCreate.volumeMeasurementType) {
            storageBinCreate.volumeMeasurementType = StorageBinVolumeMeasurementType.PIECE;
        }

        if (!storageBinCreate.volume) {
            storageBinCreate.volume = this.setDefaultVolume(storageBinCreate.volumeMeasurementType);
        }

        const result = await this.storageBin.create(storageBinCreate);
        return await result.populate('includedStorageBins', 'masterStorageBinId').execPopulate();
    }

    private async _checkIfMasterStorageBinBelongsToCompany(storageBinCreate: UpdateStorageBinDto, companyId: string) {
        const masterStorageBin = await this.storageBin.findById(storageBinCreate.masterStorageBinId);
        if (masterStorageBin && masterStorageBin.companyId !== companyId) {
            throw new NotAllowedException('StorageBin', 'includedStorageBins');
        }
    }

    public async update(id: string, updatedStorageEvent: UpdateStorageBinDto, companyId: string, session?: ClientSession): Promise<StorageBin> {
        const clientSession = session ? session : await this.transactionService.startTransactionSession();
        const existingStorageBin: InstanceType<StorageBin> = await this.getInstanceById(id, companyId, false, clientSession);
        if (updatedStorageEvent.masterStorageBinId) {
            await this._checkIfMasterStorageBinBelongsToCompany(updatedStorageEvent, companyId);
        }

        if (updatedStorageEvent.kind && updatedStorageEvent.kind !== StorageBinType.SITE) {
            updatedStorageEvent.locationId = undefined;
        }

        if (
            this._isNeedToUpdateVolumeOrVolumeType(updatedStorageEvent, existingStorageBin) &&
            this._isStorageBinOccupiedOrReserved(existingStorageBin)
        ) {
            throw new ValidationException([
                {
                    errorType: ERROR_CODES.FORBIDS_TO_UPDATE_VOLUME_IN_OCCUPIED_OR_RESERVED_STORAGE_BIN.techName,
                    entityName: 'StorageBin',
                    propertyName: 'volume',
                },
            ]);
        }

        existingStorageBin.set(updatedStorageEvent);
        await existingStorageBin.save();
        const populatedStorageBin = await existingStorageBin.populate('includedStorageBins', 'masterStorageBinId').execPopulate();

        if (!session) {
            await this.transactionService.commitTransaction(clientSession);
        }

        return populatedStorageBin;
    }

    public async getInstanceById(
        id: string,
        companyId: string,
        populateIncludedStorageBins: boolean,
        session?: ClientSession,
    ): Promise<InstanceType<StorageBin>> {
        const clientSession = session ? session : await this.transactionService.startTransactionSession();

        const storageBinWithoutIncludedBins: InstanceType<StorageBin> = await this.storageBin
            .findById(id)
            .session(clientSession)
            .exec();

        if (!isItemBelongsToCompany(storageBinWithoutIncludedBins, companyId)) {
            throw new NotAllowedException('StorageBin', 'companyId');
        }

        if (!storageBinWithoutIncludedBins) {
            throw new EntityNotFoundException('Storage bin', 'id');
        }

        let storageBin: InstanceType<StorageBin>;

        if (populateIncludedStorageBins) {
            storageBin = await storageBinWithoutIncludedBins.populate('includedStorageBins').execPopulate();
        } else {
            storageBin = await storageBinWithoutIncludedBins.populate('includedStorageBins', 'masterStorageBinId').execPopulate();
        }

        if (!session) {
            await this.transactionService.commitTransaction(clientSession);
        }
        return storageBin;
    }

    public async removeStorageBinById(id: string, companyId: string) {
        const objectToRemove: InstanceType<StorageBin> = await this.storageBin
            .findOneAndRemove({
                _id: new ObjectID(id),
                stockedResources: { $eq: [] },
                reservedStockResources: { $eq: [] },
                companyId,
            })
            .exec();

        if (!objectToRemove) {
            throw new ValidationException([
                {
                    entityName: 'StorageBin',
                    errorType: ERROR_CODES.ERROR_DURING_STORAGE_BIN_DELETING.techName,
                    propertyName: 'id',
                },
            ]);
        }
        return objectToRemove;
    }

    public async removeAllStorageBinsAssignedToCompany(companyId: string): Promise<number> {
        const objectsToRemove = await this.storageBin.remove({
            companyId,
        });

        return objectsToRemove.n;
    }

    public async getAll(navOptions: NavOptions, companyId: string): Promise<NavListDTO<{}>> {
        if (!navOptions.limit) {
            navOptions.limit = 100;
        }

        if (!navOptions.skip) {
            navOptions.skip = 0;
        }

        const queryParams = navOptions.filter || {};
        queryParams.companyId = companyId;

        const paginateOptions: PaginateOptions = {
            offset: navOptions.skip,
            limit: navOptions.limit,
            populate: [
                {
                    fieldName: 'includedStorageBins',
                    select: 'masterStorageBinId',
                },
            ],
        };
        if (navOptions.sort) {
            Object.assign(paginateOptions, { sort: navOptions.sort });
        }

        const result = await this.storageBin.paginate(queryParams, paginateOptions);

        return new NavListDTO()
            .setData(result.docs.map(itemEntity => GetStorageBinDto.fromEntity(itemEntity)))
            .setPageSize(navOptions.limit)
            .setPageNumberFromSkip(navOptions.skip)
            .setTotalSize(result.total || 0);
    }

    public async getStorageBinsWithEnoughSpaceByResouceId(resourceId: string, companyId: string) {
        const resource = await this.resource.findById(resourceId);
        if (!resource) {
            throw new EntityNotFoundException('StorageBin', 'resourceId');
        }

        if (resource.companyId !== companyId) {
            throw new NotAllowedException('Storage bin', 'resourceId');
        }

        const queryParams: any = {
            companyId,
        };
        queryParams.volumeMeasurementType = resource.volumeMeasurementType;
        queryParams.stockedResources = { $ne: resourceId };
        queryParams.reservedStockResources = { $ne: resourceId };

        queryParams.volume = { $gte: resource.volume };

        const storageBins = await this.storageBin
            .find(queryParams)
            .populate('includedStorageBins', 'masterStorageBinId')
            .exec();
        return storageBins.map(storageBin => GetStorageBinDto.fromEntity(storageBin));
    }

    public async createStorageBinsFromCSV(file: any, companyId: string): Promise<number> {
        const results = await csv({
            trim: true,
        }).fromString(file.buffer.toString('UTF-8'));
        if (!results || !results.length) {
            throw new ValidationException([
                {
                    entityName: 'Storage Bin',
                    errorType: ERROR_CODES.NON_PARSABLE_CSV_FILE.techName,
                    propertyName: 'file',
                },
            ]);
        }

        results.forEach(row => (row.id = new ObjectID().toHexString()));
        results.forEach(row => {
            const parentName = row.Parent;
            if (!parentName) {
                return;
            }
            const parentElement = results.find(innerRow => innerRow['Storage Bin'] === parentName);
            if (!parentElement) {
                throw new Error(`Unknown parent element ${parentName}`);
            }

            row.masterStorageBinId = parentElement.id;
        });

        const insertingResult = await this.storageBin.insertMany(
            results.map(row => ({
                _id: row.id,
                kind: row.Type,
                masterStorageBinId: row.masterStorageBinId,
                volume: Number.parseInt(row.capacity, 10) || 1,
                capacity: Number.parseInt(row.capacity, 10) || 1,
                volumeMeasurementType: StorageBinVolumeMeasurementType.PIECE,
                label: row['Storage Bin'],
                companyId,
            })),
            {
                rawResult: true,
            },
        );

        const countOfInsertedValues = (insertingResult as any).result.n;
        return countOfInsertedValues;
    }

    public async getStorageBinsByLabels(labels: string[]): Promise<StorageBin[]> {
        return await this.storageBin.find({
            label: { $in: labels },
        });
    }

    public async findStorageBinByDecisionService(
        resourceId: string,
        siteId: string,
        movementType: MovementType,
        companyId: string,
        userId: string,
        session?: ClientSession,
    ): Promise<StorageBin> {
        const clientSession = session ? session : await this.transactionService.startTransactionSession();

        const resource = await this.resource
            .findById(resourceId)
            .session(clientSession)
            .exec();
        if (!resource) {
            throw new EntityNotFoundException('StorageBin', 'resourceId');
        }

        if (resource.companyId !== companyId) {
            throw new NotAllowedException('Storage bin', 'resourceId');
        }

        const site = await this.getInstanceById(siteId, companyId, false, clientSession);
        if (!site) {
            throw new EntityNotFoundException('StorageBin', 'siteId');
        }

        if (site.companyId !== companyId) {
            throw new NotAllowedException('Storage bin', 'siteId');
        }

        const movement: CreateMovementDto = {
            movementType,
            movementInfo: {
                resourceId,
                siteStorageBinLabel: site.id.toString(),
            },
        };

        const destinationStorageBin = await this.movementService.getTargetStorageBinsByDecisionService(movement, resource, companyId, userId,
                clientSession);
        return destinationStorageBin;
    }

    public async updateStorageBinsOrders(storageBinsOrdersDto: UpdateOrderDto[], companyId: string): Promise<number> {
        const ids = storageBinsOrdersDto.map(dto => dto.id);
        if (!ids.length) {
            return 0;
        }

        const storageBinsToUpdate = await this.storageBin.find({
            _id: {
                $in: ids,
            },
        }).exec();

        if (!storageBinsToUpdate.every(storageBin => storageBin.companyId === companyId)) {
            throw new NotAllowedException('StorageBin', 'id');
        }

        const writeOps = storageBinsOrdersDto.map(storageBinOrderDto => ({
            updateOne: {
                filter: {
                    _id: storageBinOrderDto.id,
                },
                update: {
                    $set: { order: storageBinOrderDto.order },
                },
            },
        }));

        const BATCH_SIZE = 500;
        const writeOpsChunks = _.chunk(writeOps, BATCH_SIZE);
        // TODO: add transactions here
        // https://jira.mongodb.org/browse/NODE-1843
        for (const chunk of writeOpsChunks) {
            await this.storageBin.bulkWrite(chunk);
        }
        const result = await this.storageBin.bulkWrite(writeOps);
        return result.modifiedCount;
    }

    private setDefaultVolume(volumeMeasurementType: StorageBinVolumeMeasurementType) {
        switch (volumeMeasurementType) {
            case StorageBinVolumeMeasurementType.PIECE:
                return 1;
            case StorageBinVolumeMeasurementType.TEU:
                return 2;
            default:
                return;
        }
    }

    private _isStorageBinOccupiedOrReserved(existingStorageBin: InstanceType<StorageBin>) {
        return existingStorageBin.stockedResources.length > 0 || existingStorageBin.reservedStockResources.length > 0;
    }

    private _isNeedToUpdateVolumeOrVolumeType(updatedStorageEvent: UpdateStorageBinDto, existingStorageBin: InstanceType<StorageBin>) {
        return (
            (updatedStorageEvent.volume && updatedStorageEvent.volume !== existingStorageBin.volume) ||
            (updatedStorageEvent.volumeMeasurementType && updatedStorageEvent.volumeMeasurementType !== existingStorageBin.volumeMeasurementType)
        );
    }
}
